import { Authorizable } from '@ioc:Verful/Permissions/Mixins'
import { compose } from '@ioc:Adonis/Core/Helpers'
import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import { column, beforeSave, BaseModel } from '@ioc:Adonis/Lucid/Orm'

const config = { 
  permissionsPivotTable: 'user_has_permissions',
  rolesPivotTable: 'user_has_roles'
}

export default class User extends compose(BaseModel, Authorizable(config)) {

  @column({ isPrimary: true })
  public id: number

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

}

import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {

    public async register({ request, response }: HttpContextContract) {
        const userSchema = schema.create({
            email: schema.string({ trim: true }, [
                rules.email(),
                rules.unique(
                    { table: 'users', column: 'email', caseInsensitive: true, }
                )
            ]),
            password: schema.string({}, [
                rules.minLength(8),
                rules.confirmed()
            ])
        })

        const payload = await request.validate({ schema: userSchema })

        const user = await User.create(payload)

        response.status(201)

        return user

    }

    public async login({ request, response, auth }: HttpContextContract) {

        const userSchema = schema.create({
            email: schema.string({ trim: true }, [
                rules.email(),
            ]),
            password: schema.string({}, [
                rules.minLength(8),
            ])
        })

        const payload = await request.validate({ schema: userSchema })

        try {
            const token = await auth.use('api').attempt(payload.email, payload.password, {
                expiresIn: '10mins'
              })

            return token
        
        } catch {
        
            return response.unauthorized('Invalid credentials')
        
        }
    }
}

# Project Name

BASIC RESTFul API CRUD 

## Installation

npm install

create mysql database/schema

copy .env.example to .env 

modify .env 

node ace migration:run

## Usage

npm run dev

## routes list command

node ace list:route

## sample api/'s

| Method	| URL 		| Description|
| -------------| ------------- | -------- |
| GET          | http://localhost:3333/pets      | show all record |
| POST		| http://localhost:3333/pets      | Create new data |
| GET          | http://localhost:3333/pets/:id | Show data by id  |  
| PUT/PATCH          | http://localhost:3333/pets/:id | update data by id |
| DELETE          | http://localhost:3333/pets/:id | delete data by id |
    
## UPDATE

	IMPLEMENTED JWT 7/22

## SOON
	AUTHENTICATION ACL / ROLES AND PERMISSIONS
